package com.oos;

import com.oos.objects.Experiment;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.LinkedList;

/**
 * Created by Ondrej on 8.4.2016.
 */
public class ProgramState implements Serializable {

    private static final Logger logger = Logger.getLogger(ProgramState.class);

    private static File dataSource = new File("oos.data");

    public LinkedList<Experiment> experiments = new LinkedList<Experiment>();
    public int selectedItem = 0;

    public int getIndexOfExperiment(Experiment experiment) {
        return experiments.indexOf(experiment);
    }

    public Experiment getSelectedExperiment() {
        return experiments.get(selectedItem);
    }

    public void selectExperiment(Experiment experiment) {
        unSelect();
        selectedItem = experiments.indexOf(experiment);
        experiment.getListComponent().select();
    }

    public boolean selectExperiment(int idx) {
        if (idx < 0 || idx >= experiments.size()) {
            return false;
        } else {
            unSelect();
            selectedItem = idx;
            experiments.get(idx).getListComponent().select();
        }
        return true;
    }

    private void unSelect() {
        getSelectedExperiment().getListComponent().unSelect();
    }

    public void increaseSelection() {
        if (selectedItem != experiments.size() - 1) {
            selectExperiment(selectedItem + 1);
        }
    }

    public void decreaseSelection() {
        if (selectedItem != 0) {
            selectExperiment(selectedItem - 1);
        }
    }

    public void moveInOrderUp(Experiment experiment) {
        Experiment experimentCpy = experiment;
        int actPos = experiments.indexOf(experiment);
        if (actPos > 0) {
            if (selectedItem == actPos) {
                selectedItem--;
            } else if (selectedItem == actPos - 1) {
                selectedItem++;
            }
            experiments.set(actPos, experiments.get(actPos - 1));
            experiments.set(actPos - 1, experimentCpy);
        }
    }

    public void moveInOrderDown(Experiment experiment) {
        Experiment experimentCpy = experiment;
        int actPos = experiments.indexOf(experiment);
        if (actPos < experiments.size() - 1) {
            if (selectedItem == actPos) {
                selectedItem++;
            } else if (selectedItem == actPos + 1) {
                selectedItem--;
            }
            experiments.set(actPos, experiments.get(actPos + 1));
            experiments.set(actPos + 1, experimentCpy);
        }
    }

    public void removeExperiment() {
        if (experiments.isEmpty()) return;
        unSelect();
        experiments.remove(experiments.get(selectedItem));
        while (selectedItem > experiments.size() - 1) selectedItem--;
        if (!experiments.isEmpty()) {
            selectExperiment(selectedItem);
        } else {
            selectedItem = 0;
        }
    }

    public boolean store(){
        if(!dataSource.exists()) try {
            dataSource.createNewFile();
        } catch (IOException e) {
            logger.error("Unable to create backUp file.", e);
            return false;
        }
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(dataSource);
        } catch (FileNotFoundException e) {
            logger.error("Unable to create file output stream.", e);
            return false;
        }
        ObjectOutput out = null;
        try {
            out = new ObjectOutputStream(fos);
            out.writeObject(this);
        } catch (IOException e) {
            logger.error("Unable to store data to the file.", e);
            return false;
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException ex) {
                return false;
            }
            try {
                fos.close();
            } catch (IOException ex) {
                return false;
            }
        }
        return true;
    }

    public ProgramState(Object obj) {
        this.experiments = new LinkedList<>();
        this.selectedItem = 0;
    }

    public ProgramState() {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(dataSource);
        } catch (FileNotFoundException e) {
            logger.warn("No backUp file found.", e);
            return;
        }
        ObjectInput in = null;
        try {
            in = new ObjectInputStream(fis);
            ProgramState ps = (ProgramState) in.readObject();
            this.experiments = ps.experiments;
            this.selectedItem = ps.selectedItem;
        } catch (ClassNotFoundException e) {
            logger.error("Unable to unserialise backUp file.", e);
        } catch (IOException e) {
            logger.error("Unable to read backUp file.", e);
        } finally {
            try {
                fis.close();
            } catch (IOException ex) {
                logger.error("Unhandled exception.", ex);
            }
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                logger.error("Unhandled exception.", ex);
            }
        }
    }
}
