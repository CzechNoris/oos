package com.oos;

import com.oos.commons.GuiCommon;
import com.oos.gui.ListComponent;
import com.oos.gui.WorkSpace;
import com.oos.objects.Experiment;
import org.apache.log4j.Logger;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.HashSet;

/**
 * Created by Ondrej on 8.4.2016.
 */
public class OosGui extends JFrame{

    private static final Logger logger = Logger.getLogger(OosGui.class);

    private ProgramState ps = new ProgramState(null);

    private JPanel rootPanel;
    private JButton view1Button;
    private JButton view2Button;
    private JButton view3Button;
    private JButton view4Button;
    private JButton addButton;
    private JButton previousButton;
    private JButton removeButton;
    private JButton nextButton;
    private JCheckBox beamCheckBox;
    private JCheckBox upCheckBox;
    private JCheckBox downCheckBox;
    private JPanel listPanel;
    private JTabbedPane tabbedPane1;
    private JTextArea logText;

    private JPanel workSpacePanel;
    private WorkSpace workSpace;
    private Canvas workSpaceCanvas;

    public OosGui(){
        super("Optimization of skin");
        setContentPane(rootPanel);

        pack();
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
//        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                int confirmed = JOptionPane.showConfirmDialog(null,
                        "Are you sure you want to exit the program?", "",
                        JOptionPane.YES_NO_OPTION);

                if (confirmed == JOptionPane.YES_OPTION) {
                    storeProgramState();
                    dispose();
                    System.exit(0);
                }
            }
        });
        loadProgramState();
        workSpaceCanvas  = new Canvas();
        workSpacePanel.add(workSpaceCanvas, BorderLayout.CENTER);
        setVisible(true);
        workSpace = new WorkSpace(workSpaceCanvas, this);
        addActionListerners();
    }

    private void addActionListerners() {
        workSpaceCanvas.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e){
                WorkSpace.newCanvasSize.set(workSpaceCanvas.getSize());
            }
        });
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                openFile();
            }
        });
        removeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                removeExperiment();
            }
        });
        previousButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                select(ps.selectedItem - 1);
            }
        });
        nextButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                select(ps.selectedItem + 1);
            }
        });
        view1Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                writeToLog("View1 clicked...");
            }
        });
        view2Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                writeToLog("View2 clicked...");
            }
        });
        view3Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                writeToLog("View3 clicked...");
            }
        });
        view4Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                writeToLog("View4 clicked...");
            }
        });
        beamCheckBox.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                workSpace.setDispSet1(beamCheckBox.isSelected());
            }
        });
        upCheckBox.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                workSpace.setDispSet3(upCheckBox.isSelected());
            }
        });
        downCheckBox.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                workSpace.setDispSet2(downCheckBox.isSelected());
            }
        });

    }

    private void removeExperiment() {
        recreateListPanel();
        ps.removeExperiment();
        recreateListPanel();
    }

    private void openFile() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setMultiSelectionEnabled(true);
        if(fileChooser.showOpenDialog(OosGui.this) == JFileChooser.APPROVE_OPTION){
            File[] files = fileChooser.getSelectedFiles();
            HashSet<String> added = new HashSet<String>();
            String ending1 = ".f06"; String ending2 = ".bdf";
            String name = new String();
            for(File file : files){
                if(file.isFile()){
                    name = file.getName().split("\\.")[0];
                    if(!added.contains(name)){
                        added.add(name);
                        File inputFile = new File(file.getAbsolutePath().split("\\.")[0]+ending2);
                        File resultFile = new File(file.getAbsolutePath().split("\\.")[0]+ending1);
                        try {
                            Experiment experiment = new Experiment(inputFile, resultFile);
                            listernersToListComp(experiment.getListComponent());
                            ps.experiments.add(experiment);
                        } catch (Exception e1) {
                            logger.error("Parsing of experiments files: ", e1);
                            writeToLog("Unable to process Experiment " + name);
                            if(!inputFile.exists()) writeToLog(inputFile.getAbsolutePath() + " does not exist");
                            if(!resultFile.exists()) writeToLog(resultFile.getAbsolutePath() + " does not exist");
                        }
                    }
                }
            }
            recreateListPanel();
            select(ps.experiments.size() - 1);
        }
    }

    private void select(int selection) {
        if (ps.selectExperiment(selection)) {
            workSpace.setPoints(ps.getSelectedExperiment().getGridHashMap());
            workSpace.setPolygonsSet1(ps.getSelectedExperiment().getBeam());
            workSpace.setPolygonsSet2(ps.getSelectedExperiment().getDownSkin());
            workSpace.setPolygonsSet3(ps.getSelectedExperiment().getUpSkin());
//            workSpace.setSiQE(ps.getSelectedExperiment().getSiqeRecords());
        }
    }

    //TODO: may be moved somewhere else
    private void listernersToListComp(final ListComponent listComponent) {
        listComponent.item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                select(ps.getIndexOfExperiment(listComponent.getExperiment()));
            }
        });
        listComponent.upButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ps.moveInOrderUp(listComponent.getExperiment());
                recreateListPanel();
            }
        });
        listComponent.downButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ps.moveInOrderDown(listComponent.getExperiment());
                recreateListPanel();
            }
        });
    }

    private void recreateListPanel(){
        if (ps.experiments.isEmpty()) {
            listPanel.removeAll();
            listPanel.add(new JLabel("   Add some files..."), BorderLayout.CENTER);
        } else {
            JPanel gridLayout = new JPanel(new GridLayout(Math.max(ps.experiments.size(), GuiCommon.MIN_ITEM_IN_LIST), 1));
            for (Experiment experiment : ps.experiments) gridLayout.add(experiment.getListComponent());
            JScrollPane scrollPane = new JScrollPane(gridLayout);
//            scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
            listPanel.removeAll();
            listPanel.add(scrollPane, BorderLayout.CENTER);
            listPanel.revalidate();
        }

    }

    public void writeToLog(String str){
        logText.append(System.lineSeparator());
        logText.append(str);
    }

    private void storeProgramState() {
        if(ps.store()) {
            writeToLog("OoS sucessfully stored its state...");
        } else {
            writeToLog("Unable to store the program state!!!");
        }
        sleep(0);
    }

    private void loadProgramState() {
        ps = new ProgramState();
        if (ps == null) {
            ps = new ProgramState(null);
        } else {
            for (Experiment experiment : ps.experiments) listernersToListComp(experiment.getListComponent());
            recreateListPanel();
        }
    }

    private void sleep(int i) {
        try {
            Thread.sleep(i * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
