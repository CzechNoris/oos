package com.oos.gui;

import com.oos.OosGui;
import com.oos.gui.lwjgl.Diamond;
import com.oos.gui.lwjgl.Point3D;
import com.oos.gui.lwjgl.Polygon;
import com.oos.objects.experimentData.SiQE;
import com.oos.objects.experimentData.SiQERecord;
import com.oos.objects.general.Vector3f;
import org.apache.log4j.Logger;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicReference;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.util.glu.GLU.gluLookAt;
import static org.lwjgl.util.glu.GLU.gluPerspective;

/**
 * Created by Ondrej on 11.4.2016.
 */
public class WorkSpace extends JPanel {

    private static final Logger logger = Logger.getLogger(WorkSpace.class);

    private Camera camera;
    private Thread lwjglThread;
    private OosGui oosGui;

    public final static AtomicReference<Dimension> newCanvasSize = new AtomicReference<Dimension>();

    private boolean empty = true;
    private static final Diamond diamond = new Diamond(
        new Point3D(0f, -2f, 0f),
        new Point3D(-1f, 0f, 1f),
        new Point3D(1f, 0f, 1f),
        new Point3D(1f, 0f, -1f),
        new Point3D(-1f, 0f, -1f),
        new Point3D(0f, 2f, 0f)
    );

    private boolean dispSet1 = true;
    private boolean dispSet2 = true;
    private boolean dispSet3 = true;
    private LinkedList<Polygon> polygonsSet1 = new LinkedList<>();
    private LinkedList<Polygon> polygonsSet2 = new LinkedList<>();
    private LinkedList<Polygon> polygonsSet3 = new LinkedList<>();
    private HashMap<Integer, Point3D> gridHashMap = new HashMap<>();

    private float maxX = Float.MIN_VALUE; private float minX = Float.MAX_VALUE;
    private float maxY = Float.MIN_VALUE; private float minY = Float.MAX_VALUE;
    private float maxZ = Float.MIN_VALUE; private float minZ = Float.MAX_VALUE;

    private boolean leftGrabbed = false;
    private boolean rightGrabbed = false;

    public WorkSpace(final Canvas canvas, OosGui oosGui){
        this.oosGui = oosGui;
        lwjglThread = new Thread(){
            public void run() {
                try {
                    Display.setParent(canvas);
                    Display.setInitialBackground(1f, 1f, 1f);
                    Display.setResizable(true);
                    Display.create();
                } catch (LWJGLException e) {
                    e.printStackTrace();
                    Display.destroy();
                    System.exit(1);
                }
                glMatrixMode(GL_PROJECTION);
                glLoadIdentity();
                gluPerspective((float) 30, (float) canvas.getWidth()/ canvas.getHeight(), 0.001f, 500);
                glMatrixMode(GL_MODELVIEW);
                glEnable(GL_DEPTH_TEST);
                Dimension newDim;
                while(!Display.isCloseRequested()) {
                    //Resizing of window
                    newDim = newCanvasSize.getAndSet(null);
                    if (newDim != null){
                        GL11.glViewport(0, 0, newDim.width, newDim.height);
                    }
                    if(empty){
                        rotateDiamond();
                    } else {
                        GL11.glLoadIdentity();
                        //camera
                        cameraMovement();
                        handleInput();
                        render();
                    }
                    //display update
                    Display.update();
                    Display.sync(60);
                }
            }
        };
        lwjglThread.start();
    }

    private void cameraMovement() {
        try {
            gluLookAt(camera.getPos().getX(), camera.getPos().getY(), camera.getPos().getZ(),
                    camera.getCenter().getX(), camera.getCenter().getY(), camera.getCenter().getZ(),
                    camera.getUp().getX(), camera.getUp().getY(), camera.getUp().getZ());
        } catch (Exception e) {
            logger.warn("Unable to place camera.", e);
        }
    }

    private void render() {
        glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
        synchronized (this.gridHashMap) {
            for (Point3D point : gridHashMap.values()) {
                point.draw();
            }
        }
        synchronized (polygonsSet1) {
            if (dispSet1) for (Polygon polygon : polygonsSet1) polygon.draw(0f, 0f, 0f);
        }
        synchronized (polygonsSet2) {
            if (dispSet2) for (Polygon polygon : polygonsSet2) polygon.draw(0f, 1f, 0f);
        }
        synchronized (polygonsSet3) {
            if (dispSet3) for (Polygon polygon : polygonsSet3) polygon.draw(0f, 0f, 1f);
        }
    }

    private void rotateDiamond() {
        glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
        GL11.glLoadIdentity();
        GL11.glTranslatef(0f, 0.0f, -10f);
        GL11.glRotatef(diamond.getRotation(), 0f, 1f, 0f);
        diamond.draw();
        diamond.setRotation(diamond.getRotation() + 2f);
    }

    public void setPoints(HashMap<Integer, Vector3f> gridHashMap) {
        empty = true;
        synchronized (this.gridHashMap){
            this.gridHashMap = new HashMap<>();
            for(Integer id : gridHashMap.keySet()){
                this.gridHashMap.put(id, new Point3D(gridHashMap.get(id)));
                maxX = Math.max(maxX, gridHashMap.get(id).x); minX = Math.min(minX, gridHashMap.get(id).x);
                maxY = Math.max(maxY, gridHashMap.get(id).y); minY = Math.min(minY, gridHashMap.get(id).y);
                maxZ = Math.max(maxZ, gridHashMap.get(id).z); minZ = Math.min(minZ, gridHashMap.get(id).z);
            }
        }
        logger.info("To workspace added points: " + gridHashMap.size() +
                ", X in range: " + minX + " - " +maxX + ", Y in range: " + minY + " - " +maxY + ", Z in range: " + minZ + " - " +maxZ);

        camera = new Camera(new org.lwjgl.util.vector.Vector3f((maxX + minX) / 2, (maxY + minY) / 2, (maxZ + minZ) / 2), -100);
        empty = false;
    }

    public void setPolygonsSet1(HashMap<Integer, LinkedList<Integer>> polygonsSet) {
        empty = true;
        this.polygonsSet1.clear();
        for (LinkedList<Integer> grids : polygonsSet.values()) {
            Polygon polygon = new Polygon();
            for (Integer gridId : grids) {
                polygon.addPoint(gridHashMap.get(gridId));
            }
            polygonsSet1.add(polygon);
        }
        empty = false;
    }

    public void setPolygonsSet2(HashMap<Integer, LinkedList<Integer>> polygonsSet) {
        empty = true;
        this.polygonsSet2.clear();
        for (LinkedList<Integer> grids : polygonsSet.values()) {
            Polygon polygon = new Polygon();
            for (Integer gridId : grids) {
                polygon.addPoint(gridHashMap.get(gridId));
            }
            polygonsSet2.add(polygon);
        }
        empty = false;
    }

    public void setPolygonsSet3(HashMap<Integer, LinkedList<Integer>> polygonsSet) {
        empty = true;
        this.polygonsSet3.clear();
        for (LinkedList<Integer> grids : polygonsSet.values()) {
            Polygon polygon = new Polygon();
            for (Integer gridId : grids) {
                polygon.addPoint(gridHashMap.get(gridId));
            }
            polygonsSet3.add(polygon);
        }
        empty = false;
    }

    public void setSiQE(LinkedList<SiQE> siqes) {
        empty = true;
        synchronized (polygonsSet1) {
            for (SiQE siqe : siqes) {
                Polygon polygon = new Polygon();
                for (SiQERecord record : siqe.grid) {
                    polygon.addPoint(gridHashMap.get(record.gridId));
                }
                polygonsSet1.add(polygon);
            }
        }
        empty = false;
    }

    private void handleInput() {
        if(leftGrabbed) {
            int dx = Mouse.getDX(); int dy = Mouse.getDY();
            camera.planeMove(-dx, -dy);
        }
        leftGrabbed = Mouse.isButtonDown(0);
        if(rightGrabbed) {
            int dx = Mouse.getDX(); int dy = Mouse.getDY();
            camera.sphereMove(dx, dy);
        }
        rightGrabbed = Mouse.isButtonDown(1);
        camera.zoom(Mouse.getDWheel()/10);
    }

    private void click() {
        logger.info("Clicked...");
    }

    public void setDispSet1(boolean dispSet1) {
        this.dispSet1 = dispSet1;
    }

    public void setDispSet2(boolean dispSet2) {
        this.dispSet2 = dispSet2;
    }

    public void setDispSet3(boolean dispSet3) {
        this.dispSet3 = dispSet3;
    }
}
