package com.oos.gui;

import com.oos.commons.GuiCommon;
import com.oos.objects.Experiment;

import javax.swing.*;
import javax.swing.plaf.basic.BasicArrowButton;
import java.awt.*;

/**
 * Created by Ondrej on 4.4.2016.
 */
public class ListComponent extends JPanel{

    public static final Dimension listElementSize = new Dimension(170,80);
    public static final int lineCharacters = 30;

    public Experiment experiment;

    public JButton upButton = new BasicArrowButton(BasicArrowButton.NORTH);
    public JButton downButton = new BasicArrowButton(BasicArrowButton.SOUTH);
    private JButton infoButton = new JButton("i");
    public JButton item;

    public ListComponent(Experiment experiment) {
        this.experiment = experiment;

        String filename = experiment.getName();

        JLabel  fileName = new JLabel (filename.substring(0, Math.min(filename.length(), lineCharacters)));
        fileName.setOpaque(false);

        infoButton.setToolTipText("<html>File name: " + experiment.getName() + "<br>" +
        "Added: " + GuiCommon.DATE_FORMAT.format(experiment.getAdded()) + "</html>");

        JPanel btns = new JPanel(new FlowLayout());
        btns.setOpaque(false);
        btns.add(upButton);
        btns.add(downButton);
        btns.add(infoButton);
        infoButton.setEnabled(false);

        JPanel content = new JPanel(new BorderLayout());
        content.setSize(listElementSize);
        btns.setOpaque(false);
        content.add(fileName, BorderLayout.CENTER);
        content.add(btns, BorderLayout.SOUTH);

        item = new JButton();
        setPreferredSize(listElementSize);
        item.setMargin(new Insets(2, 1, 2, 1));
        item.add(content);

        setLayout(new BorderLayout());
        add(item, BorderLayout.CENTER);
        unSelect();
        validate();

    }

    public void select(){
        item.setBackground(GuiCommon.PRESSED_BUTTON);
    }

    public void unSelect(){
        item.setBackground(GuiCommon.FREE_BUTTON);
    }

    public Experiment getExperiment() {
        return experiment;
    }
}
