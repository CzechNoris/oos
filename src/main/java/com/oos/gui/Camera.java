package com.oos.gui;

import org.apache.log4j.Logger;
import org.lwjgl.util.vector.Vector3f;

/**
 * Created by Ondrej on 7.5.2016.
 */
public class Camera {

    private static final Logger logger = Logger.getLogger(Camera.class);

    //TODO: Change the camera to move just in a box

    private Vector3f pos;
    private Vector3f center;
    private Vector3f top;
    private Vector3f up;

    private Vector3f dirVec = new Vector3f();
    private Vector3f topVec = new Vector3f();
    private Vector3f rightVec = new Vector3f();
    private float distance = 0;

    private Vector3f initPos;
    private float initDirDist;

    public Camera(Vector3f pos, float initDirDist) {
        this.pos = pos;
        this.center = new Vector3f(pos.getX(), pos.getY(), pos.getZ() - initDirDist);
        this.up = new Vector3f(0f, 1f, 0f);
        top = new Vector3f(pos.getX() + this.up.x, pos.getY() + this.up.y, pos.getZ() + this.up.z);
        revalVecs();

        this.initPos = pos;
        this.initDirDist = initDirDist;
    }

    //TODO : Change the movement on sphere to do around center of gravity
    public void sphereMove(int dx, int dy){
        Vector3f diff = diffVec(dx, dy);
        Vector3f planePos = new Vector3f();
        Vector3f.add(pos, diff, planePos);
        Vector3f newSpherePos = countNewSpherePos(planePos);
        Vector3f move = new Vector3f();
        Vector3f.sub(newSpherePos, pos, move);
        Vector3f.add(pos, move, pos);
        Vector3f.add(top, move, top);
        revalVecs();
    }


    public void planeMove(int dx, int dy){
        Vector3f diff = diffVec(dx, dy);
        Vector3f.add(pos, diff, pos);
        Vector3f.add(center, diff, center);
        Vector3f.add(top, diff, top);
        revalVecs();
    }

    //TODO : Add maximum and minimum zoom
    public void zoom(int dz){
        Vector3f addDZ = new Vector3f(dirVec);
        addDZ.scale(dz);
        Vector3f.add(pos, addDZ, pos);
        Vector3f.add(top, addDZ, top);
        revalVecs();
    }

    private void revalVecs(){
        countDirVec();
        countTopVector();
        countRightVec();
    }

    private Vector3f countRightVec(){
        Vector3f.cross(dirVec, topVec, rightVec);
        rightVec.normalise();
        return rightVec;
    }

    private void countTopVector(){
        Vector3f.sub(top, this.pos, topVec);
        topVec.normalise();
    }

    private void countDirVec(){
        Vector3f.sub(center, pos, dirVec);
        distance = dirVec.length();
        dirVec.normalise();
    }

    private Vector3f diffVec(int dx, int dy){
        Vector3f addDX = new Vector3f(rightVec);
        addDX.scale(dx);
        Vector3f addDY = new Vector3f(topVec);
        addDY.scale(dy);
        Vector3f diff = new Vector3f();
        Vector3f.add(addDX, addDY, diff);
        return diff;
    }

    private Vector3f countNewSpherePos(Vector3f planePos) {
        Vector3f lineVec = new Vector3f();
        Vector3f.sub(planePos, center, lineVec);
        lineVec.normalise();
        double t = Math.sqrt(distance * distance / (lineVec.x * lineVec.x + lineVec.y * lineVec.y + lineVec.z * lineVec.z));
        return new Vector3f(center.x + (float)t * lineVec.x, center.y + (float)t * lineVec.y, center.z + (float)t * lineVec.z);
    }
    //Getters and Setters

    public Vector3f getPos() {
        return pos;
    }

    public Vector3f getCenter() {
        return center;
    }

    public Vector3f getUp() {
        return up;
    }

    public void setUp(Vector3f up) {
        this.up = up;
    }
}
