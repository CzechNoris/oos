package com.oos.gui.lwjgl;

import org.lwjgl.opengl.GL11;

/**
 * Created by Ondrej on 24.4.2016.
 */
public class Diamond {

    private Point3D x1; //bottom
    private Point3D x2;
    private Point3D x3;
    private Point3D x4;
    private Point3D x5;
    private Point3D x6; //top

    private float rotation = 0;

    public Diamond(Point3D x1, Point3D x2, Point3D x3, Point3D x4, Point3D x5, Point3D x6) {
        this.x1 = x1;
        this.x2 = x2;
        this.x3 = x3;
        this.x4 = x4;
        this.x5 = x5;
        this.x6 = x6;
    }

    public void draw(){
        GL11.glBegin(GL11.GL_TRIANGLES);
            GL11.glColor3f(1.0f,1.0f,0.0f);
            x1.simpleDraw();x2.simpleDraw();x3.simpleDraw();
            GL11.glColor3f(1.0f,0.5f,0.0f);
            x1.simpleDraw();x3.simpleDraw();x4.simpleDraw();
            GL11.glColor3f(1.0f,0.0f,0.0f);
            x1.simpleDraw();x4.simpleDraw();x5.simpleDraw();
            GL11.glColor3f(0.0f,1.0f,0.0f);
            x1.simpleDraw();x5.simpleDraw();x2.simpleDraw();
            GL11.glColor3f(1.0f,1.0f,0.0f);
            x6.simpleDraw();x2.simpleDraw();x3.simpleDraw();
            GL11.glColor3f(1.0f,0.5f,0.0f);
            x6.simpleDraw();x3.simpleDraw();x4.simpleDraw();
            GL11.glColor3f(1.0f,0.0f,0.0f);
            x6.simpleDraw();x4.simpleDraw();x5.simpleDraw();
            GL11.glColor3f(0.0f,1.0f,0.0f);
            x6.simpleDraw();x5.simpleDraw();x2.simpleDraw();
        GL11.glEnd();
    }

    public float getRotation() {
        return rotation;
    }

    public void setRotation(float rotation) {
        this.rotation = rotation%360;
    }
}
