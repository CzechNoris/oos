package com.oos.gui.lwjgl;

import org.lwjgl.opengl.GL11;

import java.util.Collection;
import java.util.LinkedList;

/**
 * Created by Ondrej on 25.4.2016.
 */
public class Polygon {

    public LinkedList<Point3D> point3Ds = new LinkedList<>();

    public Polygon(Collection<Point3D> point3Ds){
        point3Ds.addAll(point3Ds);
    }

    public Polygon(){
    }

    public void addPoint(Point3D point3D){
        point3Ds.add(point3D);
    }

    public void draw(float red, float green, float blue){
        switch (point3Ds.size()){
            case 3: GL11.glBegin(GL11.GL_TRIANGLES);
                break;
            case 4: GL11.glBegin(GL11.GL_QUADS);
                break;
        }
        GL11.glColor3f(red, green, blue);
        for(Point3D point3D : point3Ds) point3D.simpleDraw();
        GL11.glEnd();
    }
}
