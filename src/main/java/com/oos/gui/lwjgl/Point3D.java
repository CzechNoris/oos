package com.oos.gui.lwjgl;

import com.oos.objects.general.Vector3f;
import org.lwjgl.opengl.GL11;

import static org.lwjgl.opengl.GL11.*;

/**
 * Created by Ondrej on 21.4.2016.
 */
public class Point3D {

    private float x;
    private float y;
    private float z;

    public Point3D(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Point3D(Vector3f vector3f) {
        this.x = vector3f.x;
        this.y = vector3f.y;
        this.z = vector3f.z;
    }

    public void draw(){
        glColor3f(0f, 0.5f, 0f);
        GL11.glBegin(GL11.GL_POINTS);
        GL11.glVertex3f(x,  y, z);
        GL11.glEnd();
    }

    public void simpleDraw(){
        GL11.glVertex3f(x,  y, z);
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getZ() {
        return z;
    }
}
