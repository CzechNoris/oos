package com.oos.gui.lwjgl;

import org.lwjgl.opengl.GL11;

/**
 * Created by Ondrej on 21.4.2016.
 */
public class Block {

    private Point3D x1;
    private Point3D x2;
    private Point3D x3;
    private Point3D x4;
    private Point3D x5;
    private Point3D x6;
    private Point3D x7;
    private Point3D x8;

    public Block(Point3D x1, Point3D x2, Point3D x3, Point3D x4, Point3D x5, Point3D x6, Point3D x7, Point3D x8) {
        this.x1 = x1;
        this.x2 = x2;
        this.x3 = x3;
        this.x4 = x4;
        this.x5 = x5;
        this.x6 = x6;
        this.x7 = x7;
        this.x8 = x8;
    }

    public void draw(){
        GL11.glBegin(GL11.GL_QUADS);
            GL11.glColor3f(1.0f,1.0f,0.0f);
            x1.simpleDraw();x2.simpleDraw();x3.simpleDraw();x4.simpleDraw(); // bottom
            GL11.glColor3f(1.0f,0.5f,0.0f);
            x1.simpleDraw();x2.simpleDraw();x6.simpleDraw();x5.simpleDraw(); // front
            GL11.glColor3f(1.0f,0.0f,0.0f);
            x2.simpleDraw();x3.simpleDraw();x7.simpleDraw();x6.simpleDraw(); // right
            GL11.glColor3f(1.0f,1.0f,0.0f);
            x1.simpleDraw();x4.simpleDraw();x8.simpleDraw();x5.simpleDraw(); // left
            GL11.glColor3f(0.0f,0.0f,1.0f);
            x4.simpleDraw();x3.simpleDraw();x7.simpleDraw();x8.simpleDraw(); // back
            GL11.glColor3f(1.0f,0.0f,1.0f);
            x5.simpleDraw();x6.simpleDraw();x7.simpleDraw();x8.simpleDraw(); // top
        GL11.glEnd();
    }
}
