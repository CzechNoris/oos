package com.oos.commons;

import java.awt.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Created by Ondrej on 4.4.2016.
 */
public class GuiCommon {

    public static final Color PRESSED_BUTTON = new Color(83, 187, 3);
    public static final Color FREE_BUTTON = new Color(76, 79, 81);

    public static final int MIN_ITEM_IN_LIST = 3;

    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("HH:mm dd.MM. yyyy");
}
