package com.oos.objects.general;

import java.io.Serializable;

/**
 * Created by Ondrej on 4.4.2016.
 */
public class Vector3f implements Serializable {

    public float x;
    public float y;
    public float z;

    public Vector3f(float x, float y, float z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector3f(){
        this(0f, 0f, 0f);
    }
}
