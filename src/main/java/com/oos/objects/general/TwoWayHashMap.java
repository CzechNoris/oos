package com.oos.objects.general;

import java.util.HashMap;

/**
 * Created by Ondrej on 11.4.2016.
 */
public class TwoWayHashMap<K extends Object, V extends Object> {

    private HashMap<K,V> forward = new HashMap<K, V>();
    private HashMap<V,K> backward = new HashMap<V, K>();

    public synchronized void add(K key, V value) {
        forward.put(key, value);
        backward.put(value, key);
    }

    public synchronized V getForward(K key) {
        return forward.get(key);
    }

    public synchronized K getBackward(V key) {
        return backward.get(key);
    }
}
