package com.oos.objects;

import com.oos.gui.ListComponent;
import com.oos.objects.experimentData.SiHSE;
import com.oos.objects.experimentData.SiQE;
import com.oos.objects.general.Vector3f;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * Created by Ondrej on 4.4.2016.
 */
public class Experiment implements Serializable{

    private static final Logger logger = Logger.getLogger(Experiment.class);

    private enum Blocks {SiHSE, SiQE}

    private File inputFile;
    private File resultFile;

    private ListComponent listComponent;

    private Date added;
    private String name;
    private String comment = new String();

    //Input file variables
    private HashMap<Integer, LinkedList<Integer>> beam = new HashMap<>();
    private HashMap<Integer, LinkedList<Integer>> downSkin = new HashMap<>();
    private HashMap<Integer, LinkedList<Integer>> upSkin = new HashMap<>();
    private HashMap<Integer, Vector3f> gridHashMap;

    //Result file variables
    private LinkedList<SiHSE> sihseRecords = new LinkedList<>();
    private LinkedList<SiQE> siqeRecords = new LinkedList<>();

    public Experiment(File inputFile, File resultFile) throws IOException {
        this.inputFile = inputFile;
        this.resultFile = resultFile;
        this.name = resultFile.getName().split("\\.")[0];
        this.added = new Date();
        processInputFile(inputFile);
        processResultFile(resultFile);
        this.listComponent = new ListComponent(this);
    }

    private void processInputFile(File inputFile) throws IOException {
        gridHashMap = new HashMap<>();
        float maxX = Float.MIN_VALUE; float minX = Float.MAX_VALUE; float maxY = Float.MIN_VALUE; float minY = Float.MAX_VALUE; float maxZ = Float.MIN_VALUE; float minZ = Float.MAX_VALUE;
        try (BufferedReader br = new BufferedReader(new FileReader(inputFile))) {
            String line;
            while (!br.readLine().contains("Pset: \"Nosnik\" will be imported as")) {
                continue;
            }
            while (!(line = br.readLine()).contains("Elements and Element Properties for region") || line == null) {
                String[] lineParsed = line.split("\\s+");
                if(lineParsed[0].equals("CQUAD4")){
                    Integer id = Integer.parseInt(lineParsed[1]);
                    beam.put(id, new LinkedList<Integer>());
                    beam.get(id).add(Integer.parseInt(lineParsed[3]));
                    beam.get(id).add(Integer.parseInt(lineParsed[4]));
                    beam.get(id).add(Integer.parseInt(lineParsed[5]));
                    beam.get(id).add(Integer.parseInt(lineParsed[6]));
                }
            }
            while (!br.readLine().contains("Pset: \"Potah_horni\" will be imported as")) {
                continue;
            }
            while (!(line = br.readLine()).contains("Elements and Element Properties for region") || line == null) {
                String[] lineParsed = line.split("\\s+");
                if (lineParsed[0].equals("CQUAD4")) {
                    Integer id = Integer.parseInt(lineParsed[1]);
                    downSkin.put(id, new LinkedList<Integer>());
                    downSkin.get(id).add(Integer.parseInt(lineParsed[3]));
                    downSkin.get(id).add(Integer.parseInt(lineParsed[4]));
                    downSkin.get(id).add(Integer.parseInt(lineParsed[5]));
                    downSkin.get(id).add(Integer.parseInt(lineParsed[6]));
                }
            }
            while (!br.readLine().contains("Pset: \"Potah_dolni\" will be imported as")) {
                continue;
            }
            while (!(line = br.readLine()).contains("Referenced Material Records") || line == null) {
                String[] lineParsed = line.split("\\s+");
                if (lineParsed[0].equals("CQUAD4")) {
                    Integer id = Integer.parseInt(lineParsed[1]);
                    upSkin.put(id, new LinkedList<Integer>());
                    upSkin.get(id).add(Integer.parseInt(lineParsed[3]));
                    upSkin.get(id).add(Integer.parseInt(lineParsed[4]));
                    upSkin.get(id).add(Integer.parseInt(lineParsed[5]));
                    upSkin.get(id).add(Integer.parseInt(lineParsed[6]));
                }
            }
            while (!br.readLine().contains("Nodes of the Entire Model")) {
                continue;
            }
            while (!(line = br.readLine()).contains("Loads for Load Case : Default") || line == null) {
                String[] lineParsed = line.split("\\s+");
                int id = Integer.parseInt(lineParsed[1]);
                float x = 0;
                float y = 0;
                float z = 0;
                if (line.startsWith("GRID*")){
                    if(lineParsed.length == 4){
                        x = Float.parseFloat(lineParsed[2]);
                        y = Float.parseFloat(lineParsed[3]);
                    } else if (lineParsed.length == 3){
                        if(lineParsed[2].startsWith("-")){
                            x = Float.parseFloat(lineParsed[2].substring(0,16));
                            y = Float.parseFloat(lineParsed[2].substring(16));
                        } else {
                            x = Float.parseFloat(lineParsed[2].substring(0,15));
                            y = Float.parseFloat(lineParsed[2].substring(15));
                        }
                    }
                    z = Float.parseFloat(br.readLine().split("\\s+")[1]);
                }else if(line.startsWith("GRID ")){
                    x = Float.parseFloat(lineParsed[2]);
                    y = Float.parseFloat(lineParsed[3]);
                    z = Float.parseFloat(lineParsed[4]);
                }

                maxX = Math.max(maxX, x); minX = Math.min(minX, x); maxY = Math.max(maxY, y); minY = Math.min(minY, y) ;maxZ = Math.max(maxZ, z); minZ = Math.min(minZ, z);
                gridHashMap.put(id, new Vector3f(x / 100, y / 100, z / 100));
            }
        }
        logger.info("From input file " + inputFile.getAbsolutePath() + " loaded point: " + gridHashMap.size() +
                ", X in range: " + minX + " - " +maxX + ", Y in range: " + minY + " - " +maxY + ", Z in range: " + minZ + " - " +maxZ);
    }

    private void processResultFile(File resultFile) throws IOException{
        boolean reading = false;
        LinkedList<String> blockLines = new LinkedList<>();
        Blocks blockId = null;
        try (BufferedReader br = new BufferedReader(new FileReader(resultFile))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (line.contains("S T R E S S E S   I N   H E X A H E D R O N   S O L I D   E L E M E N T S   ( H E X A )")) {
                    if (reading)
                        logger.error("New block start and the last was not complete...");
                    reading = true;
                    blockLines.clear();
                    blockId = Blocks.SiHSE;
                } else if(line.contains("S T R E S S E S   I N   Q U A D R I L A T E R A L   E L E M E N T S   ( Q U A D 4 )")){
                    if (reading)
                        logger.error("New block start and the last was not complete...");
                    reading = true;
                    blockLines.clear();
                    blockId = Blocks.SiQE;
                } else {
                    if (reading) {
                        if (line.startsWith("1")) {
                            switch (blockId) {
                                case SiQE:
                                    siqeRecords.addAll(SiQE.fromBlock(blockLines));
                                    break;
                                default:
                                    logger.error("Unknown ending of reading block.");
                            }
                            reading = false;
                            blockLines.clear();
                            blockId = null;
                        } else {
                            blockLines.add(line);
                        }
                    }
                }
            }
        }
        logger.info("File: " + getName() + " -- SiHSE: " + sihseRecords.size() + ", SiQE: " + siqeRecords.size());
    }

    //Getters
    public File getResultFile() {
        return resultFile;
    }

    public Date getAdded() {
        return added;
    }

    public String getName() {
        return name;
    }

    public String getComment() {
        return comment;
    }

    public File getInputFile() {
        return inputFile;
    }

    public ListComponent getListComponent() {
        return listComponent;
    }

    public LinkedList<SiHSE> getSihseRecords() {
        return sihseRecords;
    }

    public LinkedList<SiQE> getSiqeRecords() {
        return siqeRecords;
    }

    public HashMap<Integer, Vector3f> getGridHashMap() {
        return gridHashMap;
    }

    public HashMap<Integer, LinkedList<Integer>> getBeam() {
        return beam;
    }

    public HashMap<Integer, LinkedList<Integer>> getDownSkin() {
        return downSkin;
    }

    public HashMap<Integer, LinkedList<Integer>> getUpSkin() {
        return upSkin;
    }
}
