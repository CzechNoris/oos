package com.oos.objects.experimentData;

import com.oos.objects.general.Vector3f;

import java.io.Serializable;

/**
 * Created by Ondrej on 11.4.2016.
 */
public class SiQERecord implements Serializable {

    public int elementId;
    public String center;
    public int gridId;
    public float fd1;
    public float fd2;
    public Vector3f siecs1;
    public Vector3f siecs2;
    public Vector3f ps1;
    public Vector3f ps2;
    public float vm1;
    public float vm2;

}
