package com.oos.objects.experimentData;

import com.oos.objects.general.Vector3f;

import java.io.Serializable;

/**
 * Created by Ondrej on 4.4.2016.
 */
public class SiHSE implements Serializable {

    private Vector3f normal;
    private Vector3f shear;
    private Vector3f principal;
    private float menaPressure;
    private float vonMises;

    public Vector3f getNormal() {
        return normal;
    }

    public void setNormal(Vector3f normal) {
        this.normal = normal;
    }

    public Vector3f getShear() {
        return shear;
    }

    public void setShear(Vector3f shear) {
        this.shear = shear;
    }

    public Vector3f getPrincipal() {
        return principal;
    }

    public void setPrincipal(Vector3f principal) {
        this.principal = principal;
    }

    public float getMenaPressure() {
        return menaPressure;
    }

    public void setMenaPressure(float menaPressure) {
        this.menaPressure = menaPressure;
    }

    public float getVonMises() {
        return vonMises;
    }

    public void setVonMises(float vonMises) {
        this.vonMises = vonMises;
    }
}
