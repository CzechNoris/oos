package com.oos.objects.experimentData;


import com.oos.gui.lwjgl.Polygon;
import com.oos.objects.general.TwoWayHashMap;
import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.Arrays;
import java.util.LinkedList;

/**
 * Created by Ondrej on 11.4.2016.
 */
public class SiQE implements Serializable {

    private static final Logger logger = Logger.getLogger(SiQE.class);

    public SiQERecord centerRecord;
    public LinkedList<SiQERecord> grid;

    public SiQE(LinkedList<String> element) {
        try {
            centerRecord = new SiQERecord();
            String[] firstLine = element.getFirst().split("\\s+");
            String[] secondLine = element.get(1).split("\\s+");
            String[] thirdLine = element.get(2).split("\\s+");
            centerRecord.elementId = Integer.parseInt(firstLine[1]);
            centerRecord.center = firstLine[2];
            //TODO: parsing of the rest

            grid = new LinkedList<>();
            for(int i = 3; i < element.size(); i +=3){
                firstLine = element.get(i).split("\\s+");
                secondLine = element.get(i).split("\\s+");
                thirdLine = element.get(i).split("\\s+");
                SiQERecord gridRecord = new SiQERecord();
                gridRecord.gridId = Integer.parseInt(firstLine[1]);
                grid.add(gridRecord);
            }
        } catch (Exception e){
            e.printStackTrace();
            logger.error("Unparseble element od SiQE: " + Arrays.toString(element.toArray()), e);
        }

    }

    public static LinkedList<SiQE> fromBlock(LinkedList<String> block) {
        LinkedList<SiQE> siqes = new LinkedList<>();
        LinkedList<String> element = new LinkedList<>();
        for(int i = 3; i < block.size(); i++){
            if(block.get(i).startsWith("0")){
                if(element.size() > 3){
                    siqes.add(new SiQE(element));
                }
                element.clear();
            }
            element.add(block.get(i));
        }
        if(element.size() > 3){
            siqes.add(new SiQE(element));
        }
        return siqes;
    }

    public static LinkedList<Polygon> countSiQEPol(LinkedList<SiQE> siqeRecords) {
        TwoWayHashMap<SiQE, Integer> map = new TwoWayHashMap<>();
        for(SiQE siqe : siqeRecords){
            for(SiQERecord grid : siqe.grid){

            }
        }
        return new LinkedList<Polygon>();
    }
}
